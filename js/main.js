// Запуск анимации объектов на карте главной страницы

window.onload = function() {
    document.getElementById('id_map_layer_one').style.display = 'block';
    document.getElementById('id_map_layer_two').style.display = 'block';

    document.getElementById('id_map_layer_three').style.opacity = '1';
    document.getElementById('id_map_layer_town').style.opacity = '1';
    document.getElementById('id_map_layer_town01').style.display = 'block';
    document.getElementById('id_map_layer_town02').style.display = 'block';
    document.getElementById('id_map_layer_town03').style.display = 'block';
    document.getElementById('id_map_layer_town04').style.display = 'block';
    document.getElementById('id_map_layer_town05').style.display = 'block';
    document.getElementById('id_map_layer_town06').style.display = 'block';
    document.getElementById('id_content_on_map').style.display = 'block';
    document.getElementById('preload').style.opacity = '0';
}

//Сортировка городов из левого меню

function closeDescriptTown(){
    var checkbox = document.getElementsByClassName('checkbox_town_info'); //Собираем все инпуты, которые открывают\закрывают подробное описание
    var linkAnchor = document.getElementsByClassName('tw_tbl-link_anchor'); //Собираем все label
    for (i=0; i<checkbox.length; i++){
        if (checkbox[i].checked){ //Если чекбокс нажат, и форма открыта
            checkbox[i].click();  //Имитируем нажатие, и закрываем дополнительное поле
        }

    }
    for(i=0; i < linkAnchor.length; i++){ //Добавляем значение data-arrow = "0", чтобы стрелочки оказались в нужных местах
        linkAnchor[i].setAttribute('data-arrow', '0');
    }
}

//Скрытие и отображение городов

function sortTown(townNumber){
    var oneTownTable = document.getElementsByClassName('tbl_town_box'); //Ищем все блоки с информацией о городах
    var fistStart = document.getElementById('div_for_function_sortTown'); //Блок проверки на открытие всех элементов
    var countDisplayTown = 0; //Счесчик открытых городов
    var linkAnchor = document.getElementsByClassName('tw_tbl-link_anchor'); //Собираем все label
    //Раздаем атрибуты

    if (fistStart.getAttribute('data-allOpen') == 1){ //Скрываем все города, если они все показывались
        for(i=0; i < oneTownTable.length; i++){
            oneTownTable[i].setAttribute('data-display', '0');
            fistStart.setAttribute('data-allOpen', '0');
        } //Если показаны не все города, идем дальше
    }
    if (oneTownTable[townNumber].getAttribute('data-display') == 0){//Решаем скрыть или отбразить город, по которому кликнули
        oneTownTable[townNumber].setAttribute('data-display', '1');
        closeDescriptTown(); //На всякий случай скрываем подробное описание городов
    }else{
        oneTownTable[townNumber].setAttribute('data-display','0');
        closeDescriptTown(); //Когда скрываем город, заодно скрываем подробное описание
    }

    for(i=0; i < oneTownTable.length; i++){ //Проверяем не закрыли ли мы все города
        if (oneTownTable[i].getAttribute('data-display') == 1){
            countDisplayTown++;
        }
    }
    if (countDisplayTown == 0){
        for(i=0; i < oneTownTable.length; i++){
            oneTownTable[i].setAttribute('data-display', '1');
        }
        fistStart.setAttribute('data-allOpen', '1');
    }

    //В соотвествии с атрибутами показываем или скрываем город

    for(i=0; i < oneTownTable.length; i++){
        if (oneTownTable[i].getAttribute('data-display') == 0) {
            oneTownTable[i].style.maxHeight = '0';
        }else{
            oneTownTable[i].style.maxHeight = '1000px';
        }
    }
    for (i=0; i<linkAnchor.length; i++){
        if(linkAnchor[i].getAttribute('data-arrow') == 0){
            linkAnchor[i].classList.remove('tw_tbl-link_anchor--transfomArrow');
        }else{
            linkAnchor[i].classList.add('tw_tbl-link_anchor--transfomArrow');
        }
    }
}

//Перворачивалка стрелочек

function arrowTransform(obj){
    obj.classList.toggle('tw_tbl-link_anchor--transfomArrow');
    if(obj.getAttribute('data-arrow') == 0){
        obj.setAttribute('data-arrow', '1')
    }else{
        obj.setAttribute('data-arrow', '0')
    }


}

//Открывание формы авторизации
function openLogForm(){
    var logForm = document.getElementsByClassName('box_au');
    var regForm = document.getElementsByClassName('box_reg');
    var wrapper = document.querySelector('.wrapper');
    if(logForm[0].style.display == 'block'){
        logForm[0].style.display = 'none';
        regForm[0].style.display = 'none';
        wrapper.removeChild(wrapper.querySelector('.full_close_window'));
    }else{
        logForm[0].style.display = 'block';
        regForm[0].style.display = 'none';
        wrapper.innerHTML += '<div class="full_close_window" onclick="openLogForm()"></div>';

    }
}
//Открывание формы регистрации
function openRegForm(){
    var regForm = document.getElementsByClassName('box_reg');
    var logForm = document.getElementsByClassName('box_au');
    var wrapper = document.querySelector('.wrapper');
    if(regForm[0].style.display == 'block'){
        regForm[0].style.display = 'none';
        logForm[0].style.display = 'none';
        wrapper.removeChild(wrapper.querySelector('.full_close_window'));
    }else{
        wrapper.removeChild(wrapper.querySelector('.full_close_window'));
        regForm[0].style.display = 'block';
        logForm[0].style.display = 'none';
        wrapper.innerHTML += '<div class="full_close_window" onclick="openRegForm()"></div>';
    }
}
//Модальное окно, после восстановления пароля
function openRegFormAfter(){
    var regForm = document.getElementsByClassName('box_reg');
    var logForm = document.getElementsByClassName('box_au');
    var logFormAfter = document.getElementsByClassName('box_after_reg');
    var wrapper = document.querySelector('.wrapper');
    if(logFormAfter[0].style.display == 'block'){
        regForm[0].style.display = 'none';
        logForm[0].style.display = 'none';
        logFormAfter[0].style.display = 'none';
        wrapper.removeChild(wrapper.querySelector('.full_close_window'));
    }else{
        wrapper.removeChild(wrapper.querySelector('.full_close_window'));
        regForm[0].style.display = 'none';
        logForm[0].style.display = 'none';
        logFormAfter[0].style.display = 'block';
        wrapper.innerHTML += '<div class="full_close_window" onclick="openRegFormAfter()"></div>';
    }
}



//Открываем и закрываем модальные окна

function modalFullDisplay(type){
    var modalWindow = document.getElementsByClassName(type);
    var bgModalWindow = document.getElementsByClassName(type +'_bg');
    var modal_control = document.getElementsByClassName('modal_control'); //Общий класс для всех окон
    //Закрываем все открытые окна
    for (i=0; i<modal_control.length; i++){
        if (modal_control[i].style.display != 'none'){
            modal_control[i].style.display = 'none';
        }
    }
    //Открываем новые
    modalWindow[0].style.display = 'table';
    bgModalWindow[0].style.display = 'block';
}

//Проверка форм на заполненность

function inputFill(obj){
    if(obj.value==""){
        obj.style.borderColor="red";
        obj.parentNode.innerHTML += '<p class="required_field">Вы не заполнили обязательное поле</p>';
    }else{
        obj.style.borderColor="red";
    }
}
//Удаляем экран со страницы Вы выбрали

function deleteDisplay(obj){
    var nodeTableRow = obj.parentNode.parentNode;
    if(nodeTableRow.getAttribute('data-id') == 1){
        nodeTableRow.setAttribute('data-id', 2);
        var nodeTableCell = nodeTableRow.querySelectorAll('.tb_bot-table-td_text');
        for (i=0; i < nodeTableCell.length; i++){
            nodeTableCell[i].style.display = 'none';
        }
        nodeTableRow.innerHTML += '<td class="delete_message" colspan="7"> Экран удален.  <a onclick="deleteDisplay(this)" href="javascript:void(0);">Восстановить?</a></td>';

    }else{
        nodeTableRow.setAttribute('data-id', 1);

        var nodeTableCell = nodeTableRow.querySelectorAll('.tb_bot-table-td_text');
        for (i=0; i < nodeTableCell.length; i++){
            nodeTableCell[i].style.display = 'table-cell';
        }
        nodeTableRow.querySelector('.delete_message').parentNode.removeChild(nodeTableRow.querySelector('.delete_message'));

    }

}
//Ответ из формы контакты, после отправки формы
function contactFormSubmit(){
    var formBlock = document.querySelector('.contact_box_bottom');
    formBlock.innerHTML = '<h3 class="c_b_bt-h3 c_b_bt-h3--after">Спасибо, Ваше сообщение отправлено!</h3><p class="c_b_bt-p">Мы ответим вам в ближайшее время.</p>'
}