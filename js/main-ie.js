// Запуск анимации объектов на карте главной страницы

window.onload = function() {
    document.getElementById('id_map_layer_one').style.display = 'block';
    document.getElementById('id_map_layer_two').style.display = 'block';

    document.getElementById('id_map_layer_three').style.opacity = '1';
    document.getElementById('id_map_layer_town').style.opacity = '1';
    document.getElementById('id_map_layer_town01').style.display = 'block';
    document.getElementById('id_map_layer_town02').style.display = 'block';
    document.getElementById('id_map_layer_town03').style.display = 'block';
    document.getElementById('id_map_layer_town04').style.display = 'block';
    document.getElementById('id_map_layer_town05').style.display = 'block';
    document.getElementById('id_map_layer_town06').style.display = 'block';
    document.getElementById('id_content_on_map').style.display = 'block';
    document.getElementById('preload').style.opacity = '0';
}